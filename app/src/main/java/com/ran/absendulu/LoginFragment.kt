package com.ran.absendulu

import android.app.Dialog
import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import com.ran.absendulu.Common.RxBaseFragment
import com.ran.absendulu.Common.RxBus
import com.ran.absendulu.Common.Utils
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_login.*
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.net.SocketException

class LoginFragment : RxBaseFragment() {

    private var loading: Dialog? = null
    private lateinit var retrofit: Retrofit

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment

        //loading = LoadingAlert.progressDialog(this.context!!, this.activity!!)
        return inflater.inflate(R.layout.fragment_login, container, false)
    }

    override fun onResume() {
        super.onResume()

        btn_login.setOnClickListener {
            RxBus.get().send(Utils.UPLOADKERJAAN)
            /*disableBtn()
            loading?.show()
*/
            /*try{
                validateUser(user_name.toString(), password.toString())
            } catch (e : SocketException){
                loading?.dismiss()
            }*/
            //validateUser(etUsername.text.toString(),HASH.md5(etPassword.text.toString()), version_data.text.toString())
        }
    }

    /*private fun validateUser(salesCode: String, toString: String) {
        val login = LoginFragment(salesCode)
        subscriptions.add(provideLoginService().userLogin(login)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                    user ->
                if (user.code != "login_failed") {
                    RxBus.get().send(Utils.UPLOADKERJAAN)
                } else {
                    toast("${user.message}")
                }
            }, {
                    _ -> toast("connection error")
            }))
    }

    private fun enableBtn(){
        btn_login.isEnabled = true
        btn_login.background = ContextCompat.getDrawable(context!!, R.drawable.btn)
    }

    private fun disableBtn() {
        btn_login.isEnabled = false
        btn_login.setBackgroundColor(Color.parseColor("#017064"))
    }

    fun provideLoginService(): LoginService {
        val clientBuilder: OkHttpClient.Builder = Utils.buildClient()

        val retrofit = Retrofit.Builder()
            .baseUrl(Utils.DEV_ENDPOINT)
            .client(clientBuilder.build())
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io()))
            .build()
        return retrofit.create(LoginService::class.java)
    }*/
}
