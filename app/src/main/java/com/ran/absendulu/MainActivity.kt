package com.ran.absendulu

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.location.LocationManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Environment
import android.provider.Settings
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import com.github.florent37.camerafragment.CameraFragment
import com.github.florent37.camerafragment.CameraFragmentApi
import com.github.florent37.camerafragment.configuration.Configuration
import com.github.florent37.camerafragment.listeners.CameraFragmentResultListener
import com.ran.absendulu.Common.RxBaseActivity
import com.ran.absendulu.Common.RxBus
import com.ran.absendulu.Common.Utils
import com.ran.absendulu.Model.ImageData
import com.ran.absendulu.Model.ImageNum
import kotlinx.android.synthetic.main.activity_main.*
import org.jetbrains.anko.alert
import org.jetbrains.anko.noButton
import org.jetbrains.anko.toast
import org.jetbrains.anko.yesButton
import rebus.permissionutils.PermissionEnum
import rebus.permissionutils.PermissionManager
import rebus.permissionutils.PermissionUtils
import java.io.File
import java.text.DateFormat
import java.util.*

class MainActivity : RxBaseActivity() {

    val cameraFragment = CameraFragment.newInstance(Configuration.Builder().build())
    val dir = "/Pictures/MKM_Absen/"
    var currentNum:Int = 0
    val arrayImageData:Array<ImageData> = arrayOf(
        ImageData("", "", "", "", ""))

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        if (savedInstanceState == null){
            getPermission()
            manageSubscription()
            changeFragment(LoginFragment(), false, "login")
            checkDir()
        }
    }

    override fun onResume() {
        super.onResume()

        btnRecord.setOnClickListener {
            handlerCamera()
        }
    }

    private fun handlerCamera() {
        val camFragment = getCameraFragment()
        camFragment.takePhotoOrCaptureVideo(object: CameraFragmentResultListener {
            override fun onVideoRecorded(filePath: String?) {
            }

            override fun onPhotoTaken(bytes: ByteArray?, filePath: String?) {
                Utils.arrayImage[currentNum] = filePath?: ""
                arrayImageData[currentNum] = ImageData(
                    "",
                    "${getImgName()}",
                    "${Utils.lat}",
                    "${Utils.long}",
                    "${getDateTime()}"
                )
                RxBus.get().send(Utils.HIDECAM)
                changeFragment(UploadKerjaan(), false, "uploadkerjaan")
            }
        },
            Environment.getExternalStorageDirectory().toString() + dir,
            getImgName()
        )
    }

    private fun changeFragment(f: Fragment, cleanStack: Boolean, tag: String) {
        val ft = supportFragmentManager.beginTransaction()
        if(cleanStack)
            clearBackStack(supportFragmentManager)
        ft.replace(R.id.main_content, f, tag)
            .addToBackStack(null)
            .commitAllowingStateLoss()
    }

    private fun clearBackStack(fm: FragmentManager) {
        if (fm.backStackEntryCount > 0){
            val first = fm.getBackStackEntryAt(0)
            fm.popBackStack(first.id, android.app.FragmentManager.POP_BACK_STACK_INCLUSIVE)
        }
    }

    private fun manageBus(event:Any){
        when(event){

            Utils.UPLOADKERJAAN -> changeFragment(UploadKerjaan(), false, "uploadkerjaan")
            Utils.HIDECAM -> layoutCamera.visibility = View.GONE

            is ImageNum -> {
                layoutCamera.visibility = View.VISIBLE
                currentNum = event.num
                changeFragment(cameraFragment, false, "camera")

            }
            Utils.GET_LOC -> getLocation()

        }
    }

    private fun manageSubscription(){
        subscriptions.add(RxBus.get().toObservable().subscribe{
                event -> manageBus(event)
        })
    }

    private fun checkDir(){
        val imgDir = File(Environment.getExternalStorageDirectory(), dir)
        if (!imgDir.exists()) {
            imgDir.mkdirs() // <----
            Toast.makeText(this, "Folder MKM_Pictures berhasil dibuat!", Toast.LENGTH_SHORT).show()
        }
        toast("{${Environment.getExternalStorageDirectory().toString() + dir}")
    }

    fun getPermission(){
        PermissionManager.Builder()
            .permission(
                PermissionEnum.WRITE_EXTERNAL_STORAGE, //Memungkinkan aplikasi untuk menulis ke penyimpanan eksternal
                PermissionEnum.ACCESS_FINE_LOCATION, //Meminta izin aplikasi untuk mengakses lokasi yang tepat dari pengguna yang di dapat dari GPS dan lokasi jaringan
                PermissionEnum.READ_PHONE_STATE, //Memungkinkan hanya membaca akses ke negara telepon
                PermissionEnum.CAMERA) //Untuk mengakses perangkat camera
            .askAgain(false)
            .ask(this)
    }

    private fun getLocation() {
        val granted = PermissionUtils.isGranted(this, PermissionEnum.ACCESS_FINE_LOCATION)
        if (granted) {
            val locationManager = getSystemService(Context.LOCATION_SERVICE) as LocationManager
            val isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)
            val isNetworkEnabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)
            var latestLocation = getLatestLocation(LocationManager.GPS_PROVIDER, locationManager)
            //
            if (!isGPSEnabled || !isNetworkEnabled)
                showSettingsAlert()
            else
            {

                /* alert("Location is Detect", "Your location is detected. Lat : "+Utils.lat+ " Long : "+ Utils.long) {
                     yesButton {
                         it.dismiss()
                     }
                 }.show()*/
                latestLocation
                Toast.makeText(this, "Lat : "+Utils.lat+ " Long : "+ Utils.long, Toast.LENGTH_SHORT).show()
            }

            if (latestLocation.isEmpty()) {
                latestLocation = getLatestLocation(LocationManager.NETWORK_PROVIDER, locationManager)
            } else {
                getPermission()
            }
        }
    }

    private fun showSettingsAlert(){
        alert("Can not get location", "Please enable GPS and Network") {
            yesButton {
                it.dismiss()
                startActivity(Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS))
            }
        }.show()
    }

    @SuppressLint("MissingPermission")
    fun getLatestLocation(provider:String, locationManager:LocationManager):Map<String, Any>{
        var result =  mutableMapOf<String, Any>()
        try {
            val location = locationManager.getLastKnownLocation(provider)
            if (location != null) {
                Utils.lat = location.latitude.toString()
                Utils.long = location.longitude.toString()
            }
        } catch (e: Throwable){
            toast( "lokasi gagal diterima $e")
        }
        return result
    }

    //Fungsi untuk menampilkan waktu ketika foto diambil
    private fun getDateTime(): String {
        val currentTime = DateFormat.getDateTimeInstance().format(Date())
        currentTime.toString()
        if (currentTime == "") {
            Toast.makeText(this, "Failed to get Date & Time. Please retake a Picture!", Toast.LENGTH_SHORT).show()
        }
        return currentTime
    }

    //fungsi untuk random number
    fun randNumber():Int{
        return Random().nextInt(10000) + 65
    }

    //Fungsi untuk membuat nama foto
    fun getImgName():String{
        return "MKM_"+randNumber()
    }

    private fun getCameraFragment(): CameraFragmentApi {
        return  getSupportFragmentManager().findFragmentByTag(Utils.CAMERA) as CameraFragmentApi
    }

    override fun onBackPressed() {
        when (supportFragmentManager.fragments.last()) {

            is LoginFragment -> {
                alert("Keluar dari Aplikasi?") {
                    yesButton {
                        finish()
                    }
                    noButton {
                        it.dismiss()
                    }
                }.show()
            }
        }
    }
}
