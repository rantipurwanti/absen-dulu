package com.ran.absendulu

import android.content.Context
import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.ran.absendulu.Common.RxBaseFragment
import com.ran.absendulu.Common.RxBus
import com.ran.absendulu.Common.Utils
import com.ran.absendulu.Model.ImageNum
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_upload_kerjaan.*
import java.io.File

class UploadKerjaan : RxBaseFragment() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        RxBus.get().send(Utils.HIDECAM)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_upload_kerjaan, container, false)
    }

    override fun onResume() {
        super.onResume()

        take_pict1.setOnClickListener {
            RxBus.get().send(ImageNum(0))
        }

        loadImage()

    }

    private fun loadImage(){
        if (!Utils.arrayImage[0].isBlank()){
            val file = File(Utils.arrayImage[0])
            Picasso.with(context).load(file).into(take_pict1)
        }
    }
}
