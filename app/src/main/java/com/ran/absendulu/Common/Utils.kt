package com.ran.absendulu.Common

import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor

class Utils {
    companion object{

        val DEV_ENDPOINT = ""
        val UPLOADKERJAAN = "uploadkerjaan"
        val HIDECAM = "hidecam"
        val CAMERA = "camera"
        val GET_LOC = "loc"
        var lat = ""
        var long = ""

        val arrayImage:Array<String> = arrayOf("")

        fun buildClient(): OkHttpClient.Builder{
            val clientBuilder = OkHttpClient.Builder()
            val loggingInterceptor = HttpLoggingInterceptor()
            loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY)
            clientBuilder.addInterceptor(loggingInterceptor)
            return clientBuilder
        }
    }
}