package com.ran.absendulu

import com.ran.absendulu.Model.LoginRes
import io.reactivex.Observable
import retrofit2.http.Body
import retrofit2.http.POST

interface LoginService{
    @POST("sales-login")
    fun userLogin(@Body login: LoginFragment): Observable<LoginRes>
}